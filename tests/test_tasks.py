import unittest
from datetime import datetime as dt, timedelta as td

from ebot.helpers import Tasks, Task


class TestTasks(unittest.TestCase):
    """Tests for `erepublik` eLatvian aviator support."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def test_get_item(self):
        tasks = Tasks()
        time_part = dt.now().replace(second=0, microsecond=0)
        tasks["first_task"] = time_part
        time_part3 = time_part - td(minutes=3)
        tasks["third_task"] = time_part3

        self.assertEqual(tasks["first_task"].name, "first_task")
        self.assertEqual(tasks["first_task"].time, time_part)
        self.assertEqual(tasks[1].name, "third_task")
        self.assertEqual(tasks[1].time, time_part3)
        self.assertEqual(tasks["non existant"], None)

    def test_set_item(self):
        tasks = Tasks()
        time_part = dt.now().replace(second=0, microsecond=0)
        tasks["first_task"] = time_part
        tasks["first_task"] = time_part
        time_part3 = time_part - td(minutes=3)
        self.assertEqual(tasks["first_task"].time, time_part)
        tasks[0] = time_part3
        self.assertEqual(tasks[0].time, time_part3)
        self.assertRaises(TypeError, tasks.__setitem__, *(1.4, 3.14))

    def test_append_and_sort(self):
        tasks = Tasks()
        time_part1 = dt.now().replace(second=0, microsecond=0)
        time_part2 = time_part1 + td(minutes=2)
        time_part3 = time_part1 - td(minutes=3)

        t1 = Task("time_part1", time_part1)
        t2 = Task("time_part2", time_part2)
        t3 = Task("time_part3", time_part3)
        self.assertDictEqual(tasks.as_dict, {"tasks": [], "defaults": {}})

        tasks.append(t1)
        tasks.append(t2)
        tasks.append(t3)
        self.assertDictEqual(tasks.as_dict, {"tasks": [t1, t2, t3], "defaults": {}})

        result_task_list = [t3, t1, t2]
        tasks.sort()
        for idx, task in enumerate(tasks):
            self.assertEqual(task, result_task_list[idx])
        self.assertDictEqual(tasks.as_dict, {"tasks": result_task_list, "defaults": {}})

        self.assertRaises(TypeError, tasks.append, 1)
        self.assertEqual(repr(tasks), "<class Tasks: 3 in queue>")
        self.assertEqual(repr(t1), f"{time_part1.strftime('%F %T')} Time part1")

    def test_pop(self):
        tasks = Tasks()
        time_part1 = dt.now().replace(second=0, microsecond=0)
        t1 = Task("time_part1", time_part1)
        self.assertDictEqual(tasks.as_dict, {"tasks": [], "defaults": {}})
        tasks.append(t1)
        self.assertDictEqual(tasks.as_dict, {"tasks": [t1], "defaults": {}})
        poped_t1 = tasks.pop("time_part1")
        self.assertEqual(poped_t1, t1)
        self.assertDictEqual(tasks.as_dict, {"tasks": [], "defaults": {}})
        poped_t = tasks.pop("time_part1")
        self.assertEqual(poped_t, None)
        self.assertDictEqual(tasks.as_dict, {"tasks": [], "defaults": {}})

    def test_defaults(self):
        tasks = Tasks()
        tasks.set_default("some_key", "some_value")
        tasks.set_default("some_other_key", 1)
        self.assertDictEqual(tasks._defaults, {"some_key": "some_value", "some_other_key": 1})
        tasks.set_default("some_other_key", 2)
        self.assertDictEqual(tasks._defaults, {"some_key": "some_value", "some_other_key": 2})
        self.assertEqual(tasks.get_default("some_key"), "some_value")
        self.assertEqual(tasks.get_default("some_other_key"), 2)
        self.assertEqual(tasks.get_default("non_existant"), None)
        self.assertEqual(tasks.get_default("non_existant", -1), -1)
