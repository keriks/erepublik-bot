import unittest
from datetime import datetime as dt, timedelta as td
from erepublik.constants import max_datetime

from ebot.helpers import Tasks, Task, PlayerTasks


class PlayerTests(PlayerTasks):
    @property
    def next_wc_start(self):
        return max_datetime


class TestTasks(unittest.TestCase):
    """Tests for `erepublik` eLatvian aviator support."""

    def setUp(self):
        self.player = PlayerTests("email@example.com", "Pa$$word1", False)
        self.player.set_debug(True)
        self.player.energy.set_reference_time(self.player.now)

    def test_get_next_fight_energy_and_time(self):
        self.player.energy.limit = 1060
        self.player.energy.interval = 26
        self.player.details.xp = 1_294_914
        self.assertTrue(self.player.is_levelup_reachable)
        self.assertEqual(self.player._get_required_fight_energy(), 2 * (1060 - 26))

        self.player.details.xp = 1_294_814
        self.assertTrue(self.player.is_levelup_close)
        self.assertEqual(self.player._get_required_fight_energy(), 850)

        self.player.details.xp = 1_294_014
        self.assertFalse(self.player.is_levelup_reachable)
        self.assertFalse(self.player.is_levelup_close)
        self.assertEqual(self.player._get_required_fight_energy(), 75)

        self.player.details.pp = 76
        self.assertEqual(self.player._get_required_fight_energy(), 1060 * 2 - 26 * 2)

        self.player.energy.pp = 76
        self.assertEqual(self.player._get_required_fight_energy(), 1060 * 2 - 26 * 2)
