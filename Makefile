version = `python -c 'from ebot import __version__;print(__version__)'`


lint:
	isort ebot
	black ebot
	flake8 ebot


version: lint
	./run.sh version


docker-build: lint
	./run.sh dockerbuild


docker-release: lint
	./run.sh dockerrelease
