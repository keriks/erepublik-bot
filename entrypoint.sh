#!/bin/sh

set -e

echo "from entrypoint: '$@'"
echo "getent: '$(getent passwd nocando)'"

GID=$(stat -c %g /app/player)
UID=$(stat -c %u /app/player)

if [ -z $(getent passwd nocando) ]; then
  addgroup -g $GID -S nocando
  adduser -u $UID -s /bin/sh -S nocando -G nocando
fi

chown -R nocando:nocando /app/player

cd /app/player

su nocando -c exec "$@"
