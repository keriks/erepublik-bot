#!/bin/sh
set -eu
err_report() {
  echo >&2 "Script '$0' failed with status code: $1!"
  exit $1
}
trap 'ERRC=$?; [ $ERRC -eq 0 ] && exit 0 || err_report $ERRC' EXIT

_check_BOTS(){
  if test -z "$BOTS"; then
    echo >&2 "Please set environment variable 'BOTS' before running this script!"
    echo >&2 "Example: BOTS=\"Plato Lana zac_A\" $0"
    exit 1
  fi
}

_get_full_path(){
  printf -- "$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
}


SCRIPT_DIR=$(dirname $(_get_full_path $0))
cd $SCRIPT_DIR
. venv/bin/activate

ebot_version=$(./venv/bin/python -c "import ebot; print(ebot.__version__)")
erep_version=$(./venv/bin/python -c "import erepublik; print(erepublik.__version__)")

_stop_bots(){
  _check_BOTS
  for bot in $BOTS; do
    if test -e "$bot/bot.pid"; then
      if test -z "`ps -eF | grep -f "$bot/bot.pid"| grep bot_`"; then
        rm "$bot/bot.pid"
      else
        pkill -F "$bot/bot.pid"
      fi
    fi
  done
}

restart(){
  _check_BOTS
  _stop_bots

  for bot in $BOTS; do
    cd $SCRIPT_DIR/$bot && ./run.sh
  done
}

compile_version(){
  . venv/bin/activate
  echo $(which python)
  pyt_v=$(python -c "import platform;print(platform.python_version())")
  echo $pyt_v
  version=$ebot_version
  elibv=$erep_version
  commit=$(git log -1 --pretty=format:%h)
  sys_v=$(uname -s)
  cpu_a=$(uname -m)
  filename="bot_${version}__${elibv}__${pyt_v}__${sys_v}__${cpu_a}__${commit}.elf"
  _compile "$filename"
}

compile_generic(){
  filename="ebot.elf"
  set +eu
  rm "$filename.spec"
  set -eu
  _compile "$filename"
}

compile_entry() {
  if test "$1" = "version"; then
    compile_version
  elif test "$1" = "generic"; then
    compile_generic
  else
    echo &>2 "Unknown compilation kind '$1'!\nAvailabale options: version, generic"
  fi
}

_compile(){
  set +eu
  rm -fr __pycache__/ ebot/__pycache__/
  set -eu
  filename="$1"
  pyinstaller -F --distpath dist --workpath work --add-data 'LICENSE:.' -n "$filename" ebot/__main__.py
}

update(){
  _check_BOTS
  fetchupdate
  compile_version
  newver=$(ls -dtr1 $SCRIPT_DIR/dist/* | tail -1)
  _stop_bots

  for bot in $BOTS; do
      cp -lfp "$newver" $SCRIPT_DIR/$bot/bot_$bot
  done
  restart
}

aviators(){
  if test -z "$1"; then
    echo >&2 "No config file specified!\nMust run like: $0 aviator \"path/to/config/file\""
    exit 1
  elif test ! -f "$1"; then
    echo >&2 "Config file does not exist!"
    exit 1
  else
    python aviator_support.py -c "$1" > log/aviator.log
  fi
}

version(){
  if test -n "`git status --porcelain --untracked-files=no`" ; then
    echo "Please commit Your changes before updating version!"
    echo $(git status --porcelain --untracked-files=no)
    exit 1
  else
    git pull
    new_version=$(python -c "exec(\"\"\"import datetime, re;from ebot import __version__;n=datetime.date.today();i=0\ntry:\n    v=re.search(r'(?P<year>\\d+)\\.(?P<month>\\d{1,2})\\.(?P<iter>\\d+)',__version__)\n    i=int(v.group('iter'))\n    if n.year==int(v.group('year'))and n.month==int(v.group('month')):i+=1\n    else: i=1\nexcept: i=1\nfinally: print(f'{n.year}.{n.month}.{i}')\"\"\")")
    echo $new_version

    sed -i.bak -E 's|^ARG version=.*$|ARG version='${new_version}'|g' Dockerfile
    rm Dockerfile.bak

    sed -i.bak -E 's|__version__ = ".+"|__version__ = "'${new_version}'"|g' ebot/__init__.py
    rm ebot/__init__.py.bak

    sed -i.bak -E "s|bot_v.* ebot|bot_v${new_version} ebot|g" compile.bat
    rm compile.bat.bak

    sed -i.bak -E "s|version=\"[0-9]{4}\.[0-9]{1,2}.[0-9]{1,}\"|version=\"${new_version}\"|g" setup.py
    rm setup.py.bak
    git add ebot/__init__.py compile.bat setup.py Dockerfile
    git commit -m "⬆ New version - ${new_version} ⬆"
    git tag "v${new_version}"
  fi
}

fetchupdate(){
  git pull
  pip install -r requirements-dev.txt
}

initialize(){
  name=$(echo "$1" | iconv -t ascii//TRANSLIT | sed -r s/[^a-zA-Z0-9]+/-/g | sed -r s/^-+\|-+$//g | tr A-Z a-z)
  mkdir -p "$name"
  python -c "import json;from ebot.utils import get_config;conf=get_config('$name/config.json');f=open('$name/config.json', 'w');json.dump(conf, f);f.close();"
  echo "#!/bin/sh\n./bot_$name &\ndisown -h %1\n" > "$name/run.sh"
  chmod +x "$name/run.sh"
  newver=$(ls -dtr1 $SCRIPT_DIR/dist/* | tail -1)
  cp -lfp "$newver" "$SCRIPT_DIR/$name/bot_$name"
}

dockerbuild(){
  docker build --build-arg version="$ebot_version" --tag ebot --tag registry.72.lv/ebot:$ebot_version --tag registry.72.lv/ebot:latest .
}

dockerpush(){
  docker push -a registry.72.lv/ebot
}

dockerrelease(){
  set -e
  if test -n "`git status --porcelain --untracked-files=no`"; then
    echo "Please commit before release!"
    exit 1
  fi
  dockerbuild
  dockerpush
}


help(){
  echo "Main entry script for working with script setup and code"
  echo ""
  echo "Available commands:"
  echo "     start|restart           Start or restart all eBot instances"
  echo "     stop                    Stop all eBot instances"
  echo "     update                  Fetch and compile newest code and update all eBots "
  echo "     version                 Increment eBot version"
  echo "     pull                    Fetch newest code and install dependencies"
  echo "     aviator 'config.json'   Run aviator support script"
  echo "     initialize 'player'     Initialize eBot account"
  echo "     dockerbuild             Build and publish fresh docker image"
  echo ""
}

case "${1:-}" in
    "start"|"restart") restart;;
    "stop") _stop_bots;;
    "update") update;;
    "version") version;;
    "pull") fetchupdate;;
    "initialize") initialize "$2";;
    "aviator") aviators "$2";;
    "help") help;;
    "dockerbuild") dockerbuild;;
    "dockerrelease") dockerrelease ;;
    "compile") compile_entry "${2:-version}";;
    *) echo "Unknown command '${1:-}'!"; help; exit 1;;
esac

